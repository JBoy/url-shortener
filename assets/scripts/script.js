$(function () {

  var $longurl      = $('input.longurl'),
      $shortend     = $('.shortend-url span'),
      $button       = $('button'),
      $toClipboard  = $('#copy-to-clipboard');

  $longurl.focus();
  $button.on('click', function () {
    sendData();
  });

  $longurl.on('keyup', function (e) {
    var key = e.which;
    $longurl.attr('placeholder', 'Shorten url pleeeeease...').parent().removeClass('error');
    $toClipboard.css('display', 'none');
    if($longurl.val().length > 2){
      $button.removeAttr('disabled');
      if(key === 13){
        sendData();
      }
    }
    else {
      $button.attr('disabled', 'disabled');
    }
  });


  function sendData() {
      if ($longurl.val() !== "") {
        $.ajax({
          type: 'POST',
          url: "/addurl",
          data: {'long_url': $longurl.val()},
          dataType: 'json',
          async: false,
          success: function (data) {
            if (data.success === 1 || data.success === 2) {
              // append val
              $shortend.empty().text($shortend.data('originalurl').trim() + data.returndata.short_url.trim()).removeClass('shortened').addClass('shortened');
              $longurl.val('');
              select_all(document.querySelector('.shortend-url span'));
              $longurl.attr('placeholder', 'Well done, your url is shortened, now click copy to clipboard.');
              $toClipboard.css('display', 'inline-block');

            }
            else if (data.success === 0) {
              $longurl.parent().addClass('error');
              setTimeout(function(){
                $longurl.parent().removeClass('error');
                $toClipboard.css('display', 'none');
              }, 2000);
            }
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log('errorThrown: ' + errorThrown);
            console.log('textStatus: ' + textStatus);
            console.log(jqXHR);
          }
        });
      }
  }

  var client = new ZeroClipboard( document.getElementById("copy-to-clipboard") );

  client.on( "ready", function( readyEvent ) {
    // alert( "ZeroClipboard SWF is ready!" );

    client.on( "aftercopy", function( event ) {
      $longurl.attr('placeholder', 'Done & Done... Your url is copied to your clipboard').parent().removeClass('error');
      // event.data["text/plain"]
    });
  });

});


$(document).on('click', '.shortened', function(){
  select_all(this);
});

function select_all(el) {
  if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
    var range = document.createRange();
    range.selectNodeContents(el);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  } else if (typeof document.selection != "undefined" && typeof document.body.createTextRange != "undefined") {
    var textRange = document.body.createTextRange();
    textRange.moveToElementText(el);
    textRange.select();
  }
}