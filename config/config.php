<?php
use RedBean_Facade as R;

$isLocalhost = ($_SERVER['REMOTE_ADDR'] == '127.0.0.1');
$dbenv = ($isLocalhost) ? 'local':'remote';
$useDB = TRUE;

if($isLocalhost){
	error_reporting(E_ALL);
}

if($useDB){
	$dbSetup = array(
		'local' => array(
			'host' => 'localhost',
			'dbname' => 'urlshortner',
			'dbuser' => 'root',
			'dbpass' => 'root'
		),
		'remote' => array(
			'host' => 'common-126697.mysql.binero.se',
			'dbname' => '126697-common',
			'dbuser' => '126697_qh80658',
			'dbpass' => 'iso12800'
		)
	);

	R::setup('mysql:host='. $dbSetup[$dbenv]['host'] .';dbname='. $dbSetup[$dbenv]['dbname'],$dbSetup[$dbenv]['dbuser'],$dbSetup[$dbenv]['dbpass']);
}
