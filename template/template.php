<!DOCTYPE html>
<html lang="sv">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="description" content="Url shortner">
  <link rel="shortcut icon" href="/assets/img/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/assets/img/favicon.ico" type="image/x-icon">

  <title>Url shortner</title>

  <!-- Bootstrap core CSS -->
  <link href="bower_components/bootstrap-css/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/styles/style.css" rel="stylesheet">
</head>

<body>

  <div class="container">
    <div class="jumbotron">
      <h1 title="yes I'm hotpink">Shorten my url</h1>
      <div class="input-group">
        <input type="url" class="form-control longurl" placeholder="Shorten url pleeeeease...">
        <span class="input-group-btn">
          <button type="submit" class="btn btn-default" disabled type="button">Go!</button>
        </span>
      </div><!-- /input-group -->
      <code class="shortend-url"><span id="ctc" data-originalurl="<?php echo $baseUrl; ?>/url/"><?php echo $baseUrl; ?>/url/</span></code>
      <div id="copy-to-clipboard" data-clipboard-target="ctc" name="copy-to-clipboard">Copy to clipboard</div>
    </div>

  </div> <!-- /container -->

  <script src="/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="/bower_components/zeroclipboard/dist/ZeroClipboard.min.js"></script>
  <script src="/assets/scripts/script.js"></script>
</body>
</html>
