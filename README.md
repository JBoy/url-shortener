# Url Shortner


This app is a setup of the following:

- Slim PHP Framework
    [Docs](http://docs.slimframework.com/)

- Redbean ORM DB
    [Docs](http://redbeanphp.com/crud)

- Twitter Bootstrap
    [Docs](http://getbootstrap.com/components/)

- Bower package manager
    [Docs](http://bower.io/)
    
- Zero clipboard
    [Zeroclipboard](http://zeroclipboard.org/)


## Features

* Shortens Url (duuh)
* Tracks how many visits
* Tracks how many times a certain url has been shortened
* BE url validation
* Mobile frendly
* Copy to clipboard functionality
* Setting to control for how long time a url can be valid *(not implemented yet)*


### System Requirements

* **PHP >= 5.3.0**.
* **Bower**


## Install



*  `git clone git@bitbucket.org:JBoy/url-shortener.git && cd url-shortner`
*  `./install.sh`
*  Create a database and run below sql
*  If you run this on localhost, be sure to setup a vhost.

```
CREATE TABLE `url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `long_url` tinytext NOT NULL,
  `short_url` varchar(60) NOT NULL DEFAULT '',
  `visits` int(11) DEFAULT '0',
  `last_visit` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `expire_on` datetime DEFAULT NULL,
  `shortened` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
```


*  Head into:
`config/config.php`

~~~php
- Set $useDB = TRUE
- Update $dbSetup arrays for local and remote credentials
~~~
