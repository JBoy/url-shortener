<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */

require 'vendor/autoload.php';
require 'vendor/slim/slim/Slim/Slim.php';
include 'config/config.php';
use RedBean_Facade as R;
\Slim\Slim::registerAutoloader();


/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim(array(
	'debug' => true,
	'templates.path' => 'template'
));


$app->hook('slim.before', function () use ($app) {
	$app->view()->appendData(array('baseUrl' => "http://". $_SERVER['HTTP_HOST'] ));
});


$app->get(
	'/',
	function () use ($app) {
		$app->render('template.php', array('stuff' => 'stuff'));


	}
);



// POST route
$app->post('/addurl',
	function () use ($app) {
		$rawUrl = $app->request()->params('long_url');
		if(!validUrl($rawUrl)){
			echo json_encode(array(
				'success' => 0,
				'message' => 'The url: \''.$rawUrl.'\' is not a valid url.'
			));
			die();
		}
		$shortUrl = generateShortUrl($rawUrl);
		$doesExist = (bool) R::getCell( 'SELECT count(*) as c FROM url WHERE short_url = ?', [ "$shortUrl" ]);
		$return = array();

		if(!$doesExist){
			$url = R::dispense('url');
			$url->long_url = $rawUrl;
			$url->short_url = $shortUrl = generateShortUrl($rawUrl);
			$url->desc = $app->request()->params('description');
			$url->created_at = date('Y-m-d H:i:s');
			$id = R::store($url);
			$return['returndata']['inserted_id'] = $id;
			$return['success'] = 1;
		}
		else {
			$return['success'] = 2;
			$url = R::findOne('url',' WHERE short_url = ?', array( $shortUrl ));
			$url->shortened++;
			R::store($url);
		}
		$return['returndata']['short_url'] =  $shortUrl;
		echo json_encode($return);
		die();
	}
);



$app->get('/url/:hash', function ($hash) use ($app) {
	$url = R::findOne('url',' WHERE short_url = ?', array( $hash ));
	$url->visits = $url->visits + 1;
	$url->last_visit = date('Y-m-d H:i:s');
	R::store($url);
	$app->redirect($url->long_url);
});


$app->get('/url|/url/', function () use ($app) {
	$app->redirect('/');
});




function generateShortUrl($rawUrl){
	return substr(sha1($rawUrl), 0,6);
}


function validUrl($url){
	return !filter_var($url, FILTER_VALIDATE_URL) === FALSE;
}


$app->run();
